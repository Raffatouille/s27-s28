const protocol = require('http');
const port = 4000;

protocol.createServer((req, res) => {
    res.write('Server is Running');

    res.end();
}).listen(port)

console.log('Initializing...');
